alias ls='ls -ahl --color=auto'

export PATH="\
/usr/bin/vendor_perl:\
/usr/bin/core_perl:\
$HOME/.local/bin:\
$HOME/bin:\
$PATH\
"

export EDITOR="emacsclient -c"
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpg-connect-agent updatestartuptty /bye > /dev/null

export QT_QPA_PLATFORMTHEME=qt5ct

# export npm_config_prefix="$HOME/.local"

. /usr/share/nvm/init-nvm.sh
